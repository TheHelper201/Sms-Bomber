"""Различные инструменты: очистка терминала, логотип и т.д."""

import os
import platform
import webbrowser
import requests

from colorama import Fore

def clear_function(OS=platform.system()):
    """Функция очитски терминала."""
    if OS == "Windows":  # На "Windows"
        os.system("cls")
    elif OS == "Linux":  # На "Linux"
        os.system("clear_function")


def banner_function(ServicesNow=7, VersionNow=1.1):
    """Логотип."""
    clear_function()

    logo = f"""
=================================================

Sms & Call Bomber                             
Version: {VersionNow}
Services: {ServicesNow}
Author: https://codeberg.org/TheHelper201

=================================================
"""
    print(Fore.GREEN + logo + "\n")


def banner_utils():
    """Меню выбора."""
    print(f"{Fore.YELLOW}1 - Начать спам\n2 - Если нашли баг\n9 - Выход{Fore.RESET}\n")


def check_connect():
    """Проверка соединения с интернетом."""
    
    site = "https://www.google.com/"
    try:
        print(f"{Fore.GREEN}Проверка соединения...")
        requests.get(site, timeout=3)  # Даём запрос на сайт дабы проверить подключение
        clear_function()
    except Warning:
        clear_function()
        print(f"{Fore.RED}Нет соединения с интернетом, попробуйте позже.{Fore.RESET} \n")
        exit()

def bug_send():
    """Мой телеграм для принития жалоб о багах и ошибках."""
    print(
        Fore.RESET + "В случае ошибок в работе программы пишите на этот телеграм \"https://t.me/ObyichniyChelovek\". Всё прочту и постараюсь иcправить.\n")
    print("1 - Перейти")
    print("2 - В меню\n")
    change = input()
    while True:
        if change == "1":
            webbrowser.open("https://t.me/TheHelper201")
            banner_function()
            banner_utils()
            break
        elif change == "2":
            clear_function()
            banner_function()
            banner_utils()
            break
        else:
            print(f"{Fore.RESET}В случае ошибок в работе программы пишите на этот телеграм \"https://t.me/TheHelper201\". Всё прочту и постараюсь иcправить.")
            print("1 - Перейти")
            print("2 - В меню")
            change = input("Введите вариант корректно: ")
