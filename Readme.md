# ✉️Sms Bomber

![image](img/Image1.png)

<br />

## ⬇ Установка

### 📱Termux/Android

```
 apt update && apt upgrade && pkg install git && pkg install python
 git clone https://codeberg.org/TheHelper201/Sms-Bomber && pip install --upgrade pip && cd Sms-Bomber && pip install -r requirements.txt && python main.py
 ```

### ♾️Перезапуск на Termux/Android

```
 cd Sms-Bomber && python main.py
 ```

<br />

### 🖥️Windows

```
 Установите Python с добавлением галочки " add python.exe to PATH ".
 Скачайте архив из репозитория " https://codeberg.org/TheHelper201/Sms-Bomber " и разархивируйте его.
 Переходите в папку с бомбером.
 Открыть командную строку или powershell.
 Проверить последнюю версию pip " pip install --upgrade pip " .
 И написать " pip install -r requirements.txt " для установки всех зависимостей.
 Открыть файл командой " python main.py ".
```

### ♾️Перезапуск на Windows

```
 Переходите в папку с бомбером.
 Открыть командную строку или powershell.
 Открыть файл командой " python main.py ".
```

<br />

## Как пользоваться

### Запускаем программу по инструкции выше, выбираем 1 пункт - начать спам и вписываем номер жертвы в строчку после двоеточия. Далее ожидаем пока сообщения будут присылаться вашей жертве. Чтобы закрыть программу на "Windows" используем комбинацию клавишь "Ctrl + C", также и с "Termux" нажимаем на клавишу "Ctrl" и потом "C".

## ❤️Enjoy!
