'''Main file for launch menu'''

from colorama import Fore  # Библиотека цветов
import Utils.Tools
import Utils.Services

Utils.Tools.check_connect()  # Проверяем соединение с интернетом

while True:
    Utils.Tools.banner_function()  # Запускаем Функцию Логотипа
    Utils.Tools.banner_utils()
    try:
        tool = input()
    except KeyboardInterrupt:
        continue

    if tool == '1': # Спам
        Utils.Tools.clear_function()
        Utils.Services.spam_sms()  # Запускаем функцию
    elif tool == '2':  # ТГ для сообщения о багах
        Utils.Tools.clear_function()
        Utils.Tools.bug_send()
    elif tool == '9':  # Выход из программы
        Utils.Tools.clear_function()
        print(Fore.RESET)
        exit()
    else:  # Если введена другая цифра
        pass
